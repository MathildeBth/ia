import numpy as np

class DataPoint:
    def __init__(self, x, y, cles):
        self.x = {}
        for i in range(len(cles)):
            self.x[cles[i]] = float(x[i])
        self.y = int(y)
        self.dim = len(self.x)
        
    def __repr__(self):
        return 'x: '+str(self.x)+', y: '+str(self.y)


def load_data(filelocation):
    with open(filelocation,'r') as f:
        data = []
        attributs = f.readline()[:-1].split(',')[:-1]
        for line in f:
            z = line.split(',')
            if z[-1] == '\n':
                z = z[:-1]
            x = z[:-1]
            y = int(z[-1])
            data.append(DataPoint(x,y,attributs))
    return data
    
//proba_empirique
def proba_empirique(d):
    res = {}
    for data in d:
        classe = data.y
        if classe in res:
            res[classe] += 1 / len(d)
        else:
            res[classe] = 1 / len(d)
    return res

//question_inf
def question_inf(data_x, a, s):
return data_x[a] < s 

//split
def split(d, a, s):
d1 = []
d2 = []

for data in d:
    if question_inf(data.x, a, s):
        d1.append(data)
    else:
        d2.append(data)

return d1, d2

//list_separ_attributs
def list_separ_attributs(d,a):
res = []
for data in d:
    res.append(data.x[a])
res = list(set(res))

return res

//list_questions
def list_questions(d):
res = []
for a in d[0].x:
    res.append(list_separ_attributs(d,a))
return res

//entropie
def entropie(d):
toReturn = {}

    for data in d:
        classe = data.y
        if classe in data:
            toReturn[classe] += -data.y * np.log(data.y)
        else:
            toReturn[classe] += -data.y

    return toReturn

//gain_entropie
def gain_entropie(d,a,s):
gain = 0

    for data in d:
        gain += entropie(d) - a * entropie(data) - s * entropie(data)

//best_split
def best_split(d):
List = ["Classe","Nationalite","Age","Nombre freres/soeurs","Nombre enfants/parents a bord","Prix du ticket","Genre"]
i = 0
attribut = ""
value = 0
g = 0
temp = 0

for a in list_questions(d):
    for s in a:
        temp = gain_entropie(d,List[i],s)
        if g<temp:
            g = temp
            attribut = List[i]
            value = s
    i = i+1
    
return [attribut,value]


class Noeud:
def __init__(self, profondeur_max=np.infty):
    self.question = None
    self.enfants = {}
    self.profondeur_max = profondeur_max
    self.proba = None
    self.hauteur = 0
    
//prediction
def prediction(self, x):
        if self.question is not None:
        if x[self.question[0]] > self.question[1]:
            self.enfants['enfant2'].prediction(x)
        else:
            self.enfants['enfant1'].prediction(x)
    else:
        self.proba
        
//precision        
def precision(n, d):
percent = 0

for data in d:
    if data.x == n:
        percent = (n / len(d)) * 100

return percent

//grow
def grow(self, data):
    entrop = entropie(data)
    best_question = best_split(data)
    liste = split(data, best_question[0], best_question[1])
    
    if liste is None :
        self.proba = proba_empirique(data)
    if entrop > 0 and self.profondeur_max != 0 and len(liste[0]) >= 1 and len(liste[1]) >= 1:
        self.question = best_question
        self.enfants = {'enfant1':Noeud(self.profondeur_max-1, self.hauteur + 1), 'enfant2':Noeud(self.profondeur_max-1, self.hauteur + 1)}
    else:
        self.proba = proba_empirique(data)
