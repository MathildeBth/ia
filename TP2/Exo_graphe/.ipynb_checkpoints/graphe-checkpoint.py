import math
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# Nombre de départements + 1

sz = 96

# Graphe plan
# Le champ g.edges a le type "graphe" standard du fichier 
# dijkstra.py

class Graphe2D:

    def __init__(self, n):
        self.positions = n * [(0,0)]
        self.edges = n * [None]
        self.names = n * ['']
        for i in range(n): self.edges[i] = []

    # Distance dans le plan entre le ième et le jème sommet du
    # graphe.
    # Le facteur 1.9 convertit à peu près les pixels en kilomètres

    def distance(self, i, j):
        (x, y) = self.positions[i]
        (a, b) = self.positions[j]
        dx = x - a
        dy = y - b
        return 1.9 * math.sqrt(dx * dx + dy * dy)

# Lecture dans le fichier routes.dat des routes entre les villes
# voisines de France

def lire_routes(g):
    villes = open('routes.dat')
    for i in range(1, sz):
        s = villes.readline().split(';')
        for j in range(1, len(s)):
            try: 
                x = int(s[j])
                g.edges[i].append((x, g.distance(x, i)))
            except: pass

# Lecture dans le fichier villes.dat des coordonnées des villes
# de France

def lire_villes(g):
    villes = open('villes.dat')
    for i in range(1, sz):
        s = villes.readline().split(';')
        g.names[i] = s[1]
        try: 
            x = int(s[2])
            y = int(s[3])
            g.positions[i] = (x, y)
        except: pass

# Graphe plan des préfectures des départements français

def lire_france():
    g = Graphe2D(sz)
    lire_villes(g)
    lire_routes(g)
    return g

# Affichage de la carte de France avec villes et routes

def afficher_carte():
    carte = mpimg.imread('france.png')
    plt.imshow(carte)
    g = lire_france()
    for i in range(1, sz):
        x, y = g.positions[i]
        s = g.edges[i]
        for (j, p) in s:
            a, b = g.positions[j]
            plt.plot([x, a], [550-y, 550-b], color='black', lw=1)

# Voir la carte de France avec villes et routes

def voir_carte():
    afficher_carte()
    plt.show()

# Voir une liste de chemins entre des couples de villes

def voir_chemin_aux(g, c):
    for i in range(len(c) - 1):
        x, y = g.positions[c[i]]
        a, b = g.positions[c[i + 1]]
        plt.plot([x, a], [550-y, 550-b], color='red', lw=2)

def voir_chemins(g, cs):
    afficher_carte()
    for c in cs: 
        voir_chemin_aux(g, c)
    plt.show()

def dicNodes(nodes):
    E = {}
    for i, node in enumerate(nodes, start=0):
        if len(node) != 0:
            E[i] = node
    return E

def dijkstra(G, nd, na):
    E = dicNodes(g.edges)
    d = {}
    prec = {}
    alt = None
    for a in E:
        d[a] = np.inf
        prec[a] = None
    d[nd] = 0
    while len(E) > 0:
        u = np.inf
        minValue = np.inf
        for a in E :
            if d[a] < minValue:
                minValue = d[a]
                u = a       
        if u == na:
            return (d, prec)
        value = E[u]
        del E[u]
        for v in value:
            alt = d[u] + v[1]
            if alt < d[v[0]]:
                d[v[0]] =  alt
                prec[v[0]] = u

def Astar(g, nd, na):
    E = getNodes(g.edges)
    d = {}
    prec = {}
    alt = None
    for a in E:
        d[a] = np.inf
        prec[a] = None

    d[nd] = nd

    while len(E) > 0:
        u = np.inf
        minValue = np.inf
        for a in E:
            if d[a] < minValue:
                minValue = d[a]
                u = a

        if u == na:
            return (d, prec)

        value = E[u]
        del E[u]
        for v in value:
            alt = d[u] + v[1]
            if alt < d[v[0]]:
                E = alt
                d[v[0]] = alt
                prec[v[0]] = u
                if not (v in E):
                    d = v

def main():
    g = lire_france()
    chemin = Astar(g, 59, 62)

    print (chemin)

main()